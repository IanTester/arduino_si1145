/*
SI1145/src/SI1145.cpp - SI1145 classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "SI1145.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

uint16_t SI1145::_read16(Register reg) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(static_cast<uint8_t>(reg));
  Wire.endTransmission();

  Wire.requestFrom(_i2caddr, (size_t)2);
  _i2cerror = false;

  int data = Wire.read();
  if (data == -1) {
    _i2cerror = true;
    return 0;
  }
  uint8_t low = data;

  data = Wire.read();
  if (data == -1) {
    _i2cerror = true;
    return 0;
  }
  uint8_t high = data;

  return ((uint16_t)high << 8) | low;
}

uint8_t SI1145::_read8(Register reg) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(static_cast<uint8_t>(reg));
  Wire.endTransmission();

  Wire.requestFrom(_i2caddr, (size_t)1);
  _i2cerror = false;

  int data = Wire.read();
  if (data == -1) {
    _i2cerror = true;
    return 0;
  }

  return data;
}

void SI1145::_write16(Register reg, uint16_t val) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(static_cast<uint8_t>(reg));
  Wire.write(val & 0xff);
  Wire.write(val >> 8);
  Wire.endTransmission();
}

void SI1145::_write8(Register reg, uint8_t val) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(static_cast<uint8_t>(reg));
  Wire.write(val);
  Wire.endTransmission();
}

bool SI1145::_send_command(Command cmd) {
  _write8(Register::COMMAND, Command::NOP);

  Response res = _response();
  if (res > Response::NO_ERROR) {
    reset();
    res = _response();
  }

  _write8(Register::COMMAND, cmd);
  res = _response();
  _prev_cmd_count = _cmd_count;

  return res == Response::NO_ERROR;
}

SI1145::Response SI1145::_response(void) {
  uint8_t res = _read8(Register::RESPONSE);
  if (res & static_cast<uint8_t>(Response_mask::ERROR) == static_cast<uint8_t>(Response::NO_ERROR)) {
    _cmd_count = res & static_cast<uint8_t>(Response_mask::NO_ERROR_count);

    return Response::NO_ERROR;
  }

  return static_cast<Response>(res);
}

uint8_t SI1145::_read_param(Parameter param) {
  _write8(Register::COMMAND, static_cast<uint8_t>(Command::PARAM_QUERY) | (static_cast<uint8_t>(param) & static_cast<uint8_t>(Command_mask::PARAM_QUERY_param)));
  return _read8(Register::PARAM_RD);
}

bool SI1145::_write_param(Parameter param, uint8_t val) {
  _write8(Register::PARAM_WR, val);
  _write8(Register::COMMAND, static_cast<uint8_t>(Command::PARAM_SET) | (static_cast<uint8_t>(param) & static_cast<uint8_t>(Command_mask::PARAM_SET_param)));

  return _read8(Register::PARAM_RD) == val;
}


SI1145::SI1145(uint8_t addr) :
  _i2caddr(addr),
  _i2cerror(false),
  _cmd_count(0)
{}

bool SI1145::check(void) {
  uint8_t id = _read8(Register::PART_ID);
  if (_i2cerror)
    return false;

  return id == static_cast<uint8_t>(PART_ID::SI1145);
}

bool SI1145::begin(void) {
  if (!check())
    return false;

  reset();

  // enable UVindex measurement coefficients
  _write8(Register::UCOEF0, 0x7b);
  _write8(Register::UCOEF1, 0x6b);
  _write8(Register::UCOEF2, 0x01);
  _write8(Register::UCOEF3, 0x00);

  // disable interrupts
  _write8(Register::INT_CFG, 0);
  _write8(Register::IRQ_ENABLE, 0);

  _write_param(Parameter::ALS_IR_ADCMUX, PSx_ADCMUX::Small_IR);
  // fastest clocks, clock div 1
  _write_param(Parameter::ALS_IR_ADC_GAIN, 0);
  // take 31 clocks to measure
  _write_param(Parameter::ALS_IR_ADC_COUNTER, x_ADC_REC::x31Clock);
  // in high range mode
  _write_param(Parameter::ALS_IR_ADC_MISC, ALS_IR_ADC_MISC_mask::IR_RANGE);

  // fastest clocks, clock div 1
  _write_param(Parameter::ALS_VIS_ADC_GAIN, 0);
  // take 31 clocks to measure
  _write_param(Parameter::ALS_VIS_ADC_COUNTER, x_ADC_REC::x31Clock);
  // in high range mode (not normal signal)
  _write_param(Parameter::ALS_VIS_ADC_MISC, ALS_VIS_ADC_MISC_mask::VIS_RANGE);

  // forced measurement mode
  set_measurement_period(0);

  return true;
}

void SI1145::reset(void) {
  set_measurement_period(0);
  _write8(Register::IRQ_ENABLE, 0);
  _write8(Register::INT_CFG, 0);
  _write8(Register::IRQ_STATUS, 0xff);

  _write8(Register::COMMAND, Command::RESET);
  delay(10);

  _write8(Register::HW_KEY, HW_KEY::Proper);

  delay(10);
}

bool SI1145::set_i2c_addr(uint8_t newaddr) {
  if (!_write_param(Parameter::I2C_ADDR, newaddr))
    return false;

  if (!_send_command(Command::BUSADDR))
    return false;

  _i2caddr = newaddr;
  return true;
}

bool SI1145::set_channel_list(bool vis, bool ir, bool uv) {
  uint8_t chlist = _read_param(Parameter::CHLIST);

  if (vis)
    chlist |= static_cast<uint8_t>(CHLIST_mask::EN_ALS_VIS);
  else
    chlist &= ~static_cast<uint8_t>(CHLIST_mask::EN_ALS_VIS);

  if (ir)
    chlist |= static_cast<uint8_t>(CHLIST_mask::EN_ALS_IR);
  else
    chlist &= ~static_cast<uint8_t>(CHLIST_mask::EN_ALS_IR);

  if (uv)
    chlist |= static_cast<uint8_t>(CHLIST_mask::EN_UV);
  else
    chlist &= ~static_cast<uint8_t>(CHLIST_mask::EN_UV);

  return _write_param(Parameter::CHLIST, chlist);
}

void SI1145::set_measurement_period(uint32_t period_us) {
  uint16_t meas_rate;
  if (period_us == 0)
    meas_rate = 0;
  else if (period_us > 2047968)
    meas_rate = 0xffff;
  else
    meas_rate = period_us * 4 / 125;

  _write16(Register::MEAS_RATE0, meas_rate);
}

unsigned long SI1145::force_measurement(void) {
  if (_send_command(Command::PS_FORCE))
    return 2000;	// 2 ms

  return 0;
}

bool SI1145::wait(void) {
  Response res;
  do {
    delay(1);
    res = _response();
    if (res > Response::NO_ERROR)
      return false;
  } while (_cmd_count == _prev_cmd_count);

  return true;
}

SI1145_reading* SI1145::get_IR(SI1145_reading* reading) {
  uint16_t ir = _read16(Register::ALS_IR_DATA0);
  if (_i2cerror)
    return nullptr;

  if (reading == nullptr)
    reading = new SI1145_reading;

  reading->_set_ir(ir);

  return reading;
}

SI1145_reading* SI1145::get_Vis(SI1145_reading* reading) {
  uint16_t vis = _read16(Register::ALS_VIS_DATA0);
  if (_i2cerror)
    return nullptr;

  if (reading == nullptr)
    reading = new SI1145_reading;

  reading->_set_vis(vis);

  return reading;
}

SI1145_reading* SI1145::get_UV(SI1145_reading* reading) {
  uint16_t uv = _read16(Register::UVINDEX0);
  if (_i2cerror)
    return nullptr;

  if (reading == nullptr)
    reading = new SI1145_reading;

  reading->_set_uv(uv);

  return reading;
}


SI1145_reading::SI1145_reading() :
  _have_ir(false),
  _have_vis(false),
  _have_uv(false),
  _ir(0), _vis(0), _uv(0)
{}

void SI1145_reading::_set_ir(uint16_t ir) {
  _ir = ir;
  _have_ir = true;
}

void SI1145_reading::_set_vis(uint16_t vis) {
  _vis = vis;
  _have_vis = true;
}

void SI1145_reading::_set_uv(uint16_t uv) {
  _uv = uv;
  _have_uv = true;
}

bool SI1145_reading::haveIR(void) const {
  return _have_ir;
}

bool SI1145_reading::haveVis(void) const {
  return _have_vis;
}

bool SI1145_reading::haveUV(void) const {
  return _have_uv;
}

float SI1145_reading::IR(void) const {
  return _ir * 0.41;
}

float SI1145_reading::Vis(void) const {
  return _vis * 3.546;
}

float SI1145_reading::UV(void) const {
  return _uv * 0.01;
}
