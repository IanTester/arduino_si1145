/*
SI1145/src/SI1145.h - SI1145 classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

#define SI1145_ADDRESS 0x60

class SI1145_reading;

class SI1145 {
private:
  enum class Register : uint8_t {
    PART_ID		= 0x00,
    REV_ID		= 0x01,
    SEQ_ID		= 0x02,
    INT_CFG		= 0x03,
    IRQ_ENABLE		= 0x04,

    HW_KEY		= 0x07,
    MEAS_RATE0		= 0x08,
    MEAS_RATE1		= 0x09,

    PS_LED21		= 0x0f,
    PS_LED3		= 0x10,

    UCOEF0		= 0x13,
    UCOEF1		= 0x14,
    UCOEF2		= 0x15,
    UCOEF3		= 0x16,
    PARAM_WR		= 0x17,
    COMMAND		= 0x18,

    RESPONSE		= 0x20,
    IRQ_STATUS		= 0x21,
    ALS_VIS_DATA0	= 0x22,
    ALS_VIS_DATA1	= 0x23,
    ALS_IR_DATA0	= 0x24,
    ALS_IR_DATA1	= 0x25,
    PS1_DATA0		= 0x26,
    PS1_DATA1		= 0x27,
    PS2_DATA0		= 0x28,
    PS2_DATA1		= 0x29,
    PS3_DATA0		= 0x2a,
    PS3_DATA1		= 0x2b,
    AUX_DATA0		= 0x2c,
    UVINDEX0		= 0x2c,	// same as AUX_DATA0
    AUX_DATA1		= 0x2d,
    UVINDEX1		= 0x2d,	// same as AUX_DATA1
    PARAM_RD		= 0x2e,

    CHIP_STAT		= 0x30,

    ANA_IN_KEY_0	= 0x3B,	// Four bytes, reserved
  };

  //! Values for PART_ID register
  enum class PART_ID : uint8_t {
    SI1145		= 0x45,
    SI1146		= 0x46,
    SI1147		= 0x47,
  };

  //! Values for SEQ_ID register
  enum class SEQ_ID : uint8_t {
    Si114x_A10		= 0x08,	// MAJOR_SEQ=1, MINOR_SEQ=0
  };

  enum class INT_CFG_mask : uint8_t {
    INT_OE		= 0x01,	// INT output enable
  };

  enum class IRQ_ENABLE_mask : uint8_t {
    ALS_IE		= 0x01,

    PS1_IE		= 0x04,
    PS2_IE		= 0x08,
    PS3_IE		= 0x10,
  };

  //! This value needs to be written to HW_KEY register for proper operation
  enum class HW_KEY : uint8_t {
    Proper		= 0x17,
  };

  enum class PS_LED21_mask : uint8_t {
    LED1_I		= 0x0f,
    LED2_I		= 0xf0,
  };

  enum class PS_LED21_shift : uint8_t {
    LED1_I		= 0,
    LED2_I		= 4,
  };

  enum class PS_LED3_mask : uint8_t {
    LED3_I		= 0x0f,
  };

  enum class PS_PED3_shift : uint8_t {
    LED3_I		= 0,
  };

  enum class IRQ_STATUS_mask : uint8_t {
    ALS_INT		= 0x03,
    PS1_INT		= 0x04,
    PS2_INT		= 0x08,
    PS3_INT		= 0x10,
    CMD_INT		= 0x20,
  };

  enum class CHIP_STAT_mask : uint8_t {
    SLEEP		= 0x01,
    SUSPEND		= 0x02,
    RUNNING		= 0x04,
  };

  // All commands except BUSADDR put an error code in the RESPONSE register
  enum class Command : uint8_t {
    NOP			= 0x00,
    RESET		= 0x01,
    BUSADDR		= 0x02,

    PS_FORCE		= 0x05,
    ALS_FORCE		= 0x06,
    PSALS_FORCE		= 0x07,

    PS_PAUSE		= 0x09,
    ALS_PAUSE		= 0x0a,
    PSALS_PAUSE		= 0x0b,

    PS_AUTO		= 0x0d,
    ALS_AUTO		= 0x0e,
    PSALS_AUTO		= 0x0f,

    // Reserved?
    GET_CAL_INDEX	= 0x11,
    GET_CAL		= 0x12,

    PARAM_QUERY		= 0x80,	// lower five bits is the parameter number

    PARAM_SET		= 0xa0,	// lower five bits is the parameter number
  };

  enum class Command_mask : uint8_t {
    PARAM_QUERY_param	= 0x1f,
    PARAM_SET_param	= 0x1f,
  };

  enum class Response : uint8_t {
    NO_ERROR		= 0x00,	// lower four bits is a counter

    INVALID_SETTING	= 0x80,

    PS1_ADC_OVERFLOW	= 0x88,
    PS2_ADC_OVERFLOW	= 0x89,
    PS3_ADC_OVERFLOW	= 0x8a,

    ALS_VIS_ADC_OVERFLOW = 0x8c,
    ALS_IR_ADC_OVERFLOW	= 0x8d,
    AUX_ADC_OVERFLOW	= 0x8e,
  };

  enum class Response_mask : uint8_t {
    ERROR		= 0xf0,
    NO_ERROR_count	= 0x0f,
  };

  //! Values for use with PARAM_QUERY and PARAM_SET commands
  enum class Parameter : uint8_t {
    I2C_ADDR		= 0x00,
    CHLIST		= 0x01,
    PSLED12_SELECT	= 0x02,
    PSLED3_SELECT	= 0x03,

    PS_ENCODING		= 0x05,
    ALS_ENCODING	= 0x06,
    PS1_ADCMUX		= 0x07,
    PS2_ADCMUX		= 0x08,
    PS3_ADCMUX		= 0x09,
    PS_ADC_COUNTER	= 0x0a,
    PS_ADC_GAIN		= 0x0b,
    PS_ADC_MISC		= 0x0c,

    ALS_IR_ADCMUX	= 0x0e,
    AUX_ADCMUX		= 0x0f,
    ALS_VIS_ADC_COUNTER	= 0x10,
    ALS_VIS_ADC_GAIN	= 0x11,
    ALS_VIS_ADC_MISC	= 0x12,

    LED_REC		= 0x1c,	// Reserved
    ALS_IR_ADC_COUNTER	= 0x1d,
    ALS_IR_ADC_GAIN	= 0x1e,
    ALS_IR_ADC_MISC	= 0x1f,
  };

  enum class CHLIST_mask : uint8_t {
    EN_PS1		= 0x01,
    EN_PS2		= 0x02,
    EN_PS3		= 0x04,

    EN_ALS_VIS		= 0x10,
    EN_ALS_IR		= 0x20,
    EN_AUX		= 0x40,
    EN_UV		= 0x80,
  };

  enum class PSLED12_SELECT_mask : uint8_t {
    PS1_LED		= 0x07,
    PS2_LED		= 0x70,
  };

  enum class PSLED12_SELECT_shift :uint8_t {
    PS1_LED		= 0,
    PS2_LED		= 4,
  };

  enum class PSLED3_SELECT_mask : uint8_t {
    PS3_LED		= 0x07,
  };

  enum class PSLED3_SELECT_shift : uint8_t {
    PS3_LED		= 0,
  };

  //! For use with PSLED12_SELECT and PSLED3_SELECT parameters, use appropriate *_shift enum classes
  enum class PSLEDx_SELECT_mask : uint8_t {
    NO_LED		= 0x00,
    LED1		= 0x01,
    LED2		= 0x02,	// Only on Si1146 and Si1147
    LED3		= 0x04,	// Only on Si1147
  };

  enum class PS_ENCODING_mask : uint8_t {
    PS1_ALIGN		= 0x10,
    PS2_ALIGN		= 0x20,
    PS3_ALIGN		= 0x40,
  };

  enum class ALS_ENCODING_mask : uint8_t {
    ALS_VIS_ALIGN	= 0x10,
    ALS_IR_ALIGN	= 0x20,
  };

  //! For use with PS1_ADCMUX, PS2_ADCMUX, and PS3_ADCMUX parameters
  enum class PSx_ADCMUX : uint8_t {
    Small_IR		= 0x00,	// Valid when PS_ADC_MODE = 1
    Visible		= 0x02,	// Valid when PS_ADC_MODE = 0
    Large_IR		= 0x03,	// Valid when PS_ADC_MODE = 1
    None		= 0x06,
    Gnd			= 0x25,
    Temperature		= 0x65,
    Vdd			= 0x75,
  };

  enum class PS_ADC_COUNTER_mask : uint8_t {
    PS_ADC_REC		= 0x70,
  };

  //! For use with PS_ADC_COUNT, VIS_ADC_COUNT, and IR_ADC_COUNTER parameters
  enum class x_ADC_REC : uint8_t {
    x1Clock		= 0x00,	// 50 ns * 2^x_ADC_GAIN
    x7Clock		= 0x10,	// 350 ns * 2^x_ADC_GAIN
    x15Clock		= 0x20,	// 750 ns * 2^x_ADC_GAIN
    x31Clock		= 0x30,	// 1.55 us * 2^x_ADC_GAIN
    x63Clock		= 0x40,	// 3.15 us * 2^x_ADC_GAIN
    x127Clock		= 0x50,	// 6.35 us * 2^x_ADC_GAIN
    x255Clock		= 0x60,	// 12.75 us * 2^x_ADC_GAIN
    x511Clock		= 0x70,	// 25.55 us * 2^x_ADC_GAIN
  };

  enum class PS_ADC_GAIN_mask : uint8_t {
    PS_ADC_GAIN		= 0x07,
  };

  enum class PS_ADC_MISC_mask : uint8_t {
    PS_ADC_MODE		= 0x04,
    PS_RANGE		= 0x20,
  };

  enum class PS_ADC_MODE : uint8_t {
    Raw_ADC		= 0x00,
    Normal_proximity	= 0x04,
  };

  enum class PS_RANGE : uint8_t {
    Normal		= 0x00,
    High		= 0x20,
  };

  enum class ALS_IR_ADCMUX : uint8_t {
    Small_IR		= 0x00,
    Large_IR		= 0x03,
  };

  enum class AUX_ADCMUX : uint8_t {
    Temperature		= 0x65,
    Vdd			= 0x75,
  };

  enum class ALS_VIS_ADC_COUNTER_mask : uint8_t {
    VIS_ADC_REC		= 0x70,
  };

  enum class ALS_VIS_ADC_GAIN_mask : uint8_t {
    ALS_VIS_ADC_GAIN	= 0x07,
  };

  enum class ALS_VIS_ADC_MISC_mask : uint8_t {
    VIS_RANGE		= 0x20,
  };

  //! For use with VIS_RANGE and IR_RANGE
  enum class x_RANGE : uint8_t {
    Normal		= 0x00,
    High		= 0x20,
  };

  enum class ALS_IR_ADC_COUNTER_mask : uint8_t {
    IR_ADC_REC		= 0x70,
  };

  enum class ALS_IR_ADC_GAIN_mask : uint8_t {
    ALS_IR_ADC_GAIN	= 0x07,
  };

  enum class ALS_IR_ADC_MISC_mask : uint8_t {
    IR_RANGE		= 0x20,
  };


  uint8_t _i2caddr;
  bool _i2cerror;
  uint8_t _cmd_count, _prev_cmd_count;

  uint16_t _read16(Register reg);
  uint8_t _read8(Register reg);
  void _write16(Register reg, uint16_t val);
  void _write8(Register reg, uint8_t val);

  template <typename T>
  void _write16(Register reg, T val) { _write16(reg, static_cast<uint16_t>(val)); }

  template <typename T>
  void _write8(Register reg, T val) { _write8(reg, static_cast<uint8_t>(val)); }

  bool _send_command(Command cmd);
  Response _response(void);

  uint8_t _read_param(Parameter param);
  bool _write_param(Parameter param, uint8_t val);

  template <typename T>
  void _write_param(Parameter param, T val) { _write_param(param, static_cast<uint8_t>(val)); }


public:
  SI1145(uint8_t addr = SI1145_ADDRESS);

  bool check(void);

  bool begin(void);

  void reset(void);

  //! Change the I2C address of the device
  bool set_i2c_addr(uint8_t newaddr);

  //! Enable/disable channels
  bool set_channel_list(bool vis, bool ir, bool uv);

  //! Set the measurement period in microseconds
  /*! The actual period is in increments of 31.25 us
   *  Use a period of 0 for Forced Measurment mode
   */
  void set_measurement_period(uint32_t period_us);

  //! Force a measurement
  unsigned long force_measurement(void);

  bool wait(void);

  SI1145_reading* get_IR(SI1145_reading* reading = nullptr);
  SI1145_reading* get_Vis(SI1145_reading* reading = nullptr);
  SI1145_reading* get_UV(SI1145_reading* reading = nullptr);

}; // class SI1145

class SI1145_reading {
private:
  bool _have_ir, _have_vis, _have_uv;
  uint16_t _ir, _vis, _uv;

  friend class SI1145;

  SI1145_reading();

  void _set_ir(uint16_t ir);
  void _set_vis(uint16_t vis);
  void _set_uv(uint16_t uv);

public:
  bool haveIR(void) const;
  bool haveVis(void) const;
  bool haveUV(void) const;

  float IR(void) const;
  float Vis(void) const;
  float UV(void) const;

}; // class SI1145_reading
